from collections import namedtuple
from typing import List

import pygame
from pygame.colordict import THECOLORS

Position = namedtuple("Position", "x, y")


class DrawableObject(object):
    _surface: pygame.Surface

    def __init__(self, surface: pygame.Surface):
        self._surface = surface

    def draw(self, color: str):
        pass


class Unit(DrawableObject):
    def __init__(self, surface: pygame.Surface, position: Position, color: str = None):
        super().__init__(surface)
        if color:
            self.__color_id = 0 if color == "white" else 1
            self.valid = True
        else:
            self.__color_id = -1
            self.valid = False

        self.__position = position

    @property
    def color(self):
        if self.valid:
            return "b" if self.__color_id else "w"
        else:
            return "e"

    def flip(self):
        if self.valid:
            self.__color_id = 1 - self.__color_id

    def draw(self, color: str = None):
        if self.valid:
            pygame.draw.circle(
                self._surface,
                pygame.color.THECOLORS["gray75" if self.__color_id else "gray5"],
                (self.__position[0] * 50 + 26, self.__position[1] * 50 + 26),
                20,
            )


class Board(DrawableObject):
    class Cursor(DrawableObject):
        def __init__(self, surface: pygame.Surface, pos: Position = Position(0, 0)):
            super().__init__(surface)
            self.x, self.y = pos
            self.__iter_pos: int = 0

        def __getitem__(self, index: int):
            if index == 0:
                return self.x
            else:
                return self.y

        def __str__(self):
            return f"({self.x}; {self.y})"

        def __iter__(self):
            return self

        def __next__(self):
            if self.__iter_pos == 0:
                self.__iter_pos += 1
                return self.x
            elif self.__iter_pos == 1:
                self.__iter_pos += 1
                return self.y
            else:
                self.__iter_pos = 0
                raise StopIteration

        def __iadd__(self, other: Position):
            x, y = other
            if self.x + x < 8 and self.y + y < 8:
                self.x += x
                self.y += y
            return self

        def __isub__(self, other: Position):
            x, y = other
            if self.x - x > -1 and self.y - y > -1:
                self.x -= x
                self.y -= y
            return self

        def draw(self, color: str):
            pygame.draw.polygon(
                self._surface,
                THECOLORS[color],
                (
                    (self.x * 50, self.y * 50),
                    (self.x * 50 + 50, self.y * 50),
                    (self.x * 50 + 50, self.y * 50 + 50),
                    (self.x * 50, self.y * 50 + 50),
                ),
                2,
            )

        def up(self):
            if self.y > 0:
                self.y -= 1

        def down(self):
            if self.y < 7:
                self.y += 1

        def left(self):
            if self.x > 0:
                self.x -= 1

        def right(self):
            if self.x < 7:
                self.x += 1

    def __init__(self, surface: pygame.Surface):
        super().__init__(surface)
        self.__board: List[List[Unit]] = [
            [Unit(self._surface, Position(i, j)) for i in range(8)] for j in range(8)
        ]
        self.__board[3][4] = Unit(self._surface, Position(3, 4), "white")
        self.__board[3][3] = Unit(self._surface, Position(3, 3), "black")
        self.__board[4][4] = Unit(self._surface, Position(4, 4), "black")
        self.__board[4][3] = Unit(self._surface, Position(4, 3), "white")
        self.score = [2, 2]
        self.cursor = self.Cursor(self._surface, Position(0, 0))
        self.__active_player = 0

    def __find_available_moves(self):
        adjacent = (
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        )
        players = ("b", "w")
        candidate_moves = []
        final_moves_list: List[Position] = []
        moves_string = players[1 - self.__active_player] + players[self.__active_player]
        for i in range(8):
            for j in range(8):
                if self[Position(i, j)].color == players[1 - self.__active_player]:
                    for x1, y1 in adjacent:
                        if not self[Position(i + x1, j + y1)].valid:
                            candidate_moves.append(Position(i + x1, j + y1))

        for x, y in candidate_moves:
            half_row = [""] * 8
            short_list = []

            for adj_x, adj_y in adjacent:
                if (
                        self[Position(x + adj_x, y + adj_y)].color
                        == players[1 - self.__active_player]
                ):
                    short_list.append(Position(adj_x, adj_y))
            for i in range(8):
                for j, sh in enumerate(short_list):
                    adj_x, adj_y = sh
                    half_row[j] += self[(x + adj_x * i, y + adj_y * i)].color
                for r in half_row:
                    if moves_string in r:
                        final_moves_list.append(Position(x, y))

        return final_moves_list

    def move_available(self):
        x, y = self.cursor
        if Position(x, y) in self.__find_available_moves():
            return True
        else:
            return False

    def handle_move(self, active_player):
        self.__active_player = active_player
        if self.move_available():
            self.update()
            self.__active_player = 1 - self.__active_player
        game_over = False
        return self.__active_player, game_over

    def draw(self, color: str):
        for i in range(50, 450, 50):
            pygame.draw.line(self._surface, THECOLORS[color], (0, i), (400, i), 2)
            pygame.draw.line(self._surface, THECOLORS[color], (i, 0), (i, 400), 2)

        self.cursor.draw("blue" if self.move_available() else "red")

        for row in self.__board:
            for unit in row:
                unit.draw()

    def update(self):
        taken_pieces = []
        candidate_row: List[List[Position]] = [[] for _ in range(8)]
        short_list = []
        adjacent = (
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        )
        players = ("b", "w")

        for adj_x, adj_y in adjacent:
            if (
                    self[self.cursor.x + adj_x, self.cursor.y + adj_y].color
                    == players[1 - self.__active_player]
            ):
                short_list.append(Position(adj_x, adj_y))
        for i in range(8):
            for j, pos in enumerate(short_list):
                adj_x, adj_y = pos
                if -1 < self.cursor.x + adj_x * i < 8 and -1 < self.cursor.y + adj_y * i < 8:
                    checking_unit: Unit = self[(self.cursor.x + adj_x * i, self.cursor.y + adj_y * i)]
                    if checking_unit.color == players[1 - self.__active_player]:
                        candidate_row[j].append(Position(
                                self.cursor.x + adj_x * i, self.cursor.y + adj_y * i
                            )
                        )

                    elif checking_unit.color == players[self.__active_player]:
                        taken_pieces.extend(candidate_row[j])
        taken_pieces = list(set(taken_pieces))
        for x, y in taken_pieces:
            self[Position(x, y)] = "white" if self.__active_player else "black"
            self.score[self.__active_player] += 1
            self.score[1 - self.__active_player] -= 1
        self[Position(self.cursor.x, self.cursor.y)] = (
            "white" if self.__active_player else "black"
        )
        self.score[self.__active_player] += 1

    def __iter__(self):
        return iter(self.__board)

    def __setitem__(self, pos: Position, color: str):
        x, y = pos
        self.__board[x][y] = Unit(self._surface, pos, color)

    def __getitem__(self, pos: Position):
        x, y = pos
        if -1 < x < 8 and y - 1 < y < 8:
            return self.__board[x][y]
        else:
            return Unit(self._surface, Position(x, y))


class _TextBoxUnit(DrawableObject):
    def __init__(
            self,
            surface: pygame.Surface,
            pos: Position,
            text_func,
            color: str,
            font: str,
            font_size: int,
    ):
        super().__init__(surface)
        self.font = pygame.font.SysFont(font, font_size)
        self.text_func = text_func
        self.color = THECOLORS[color]
        self.pos = pos
        self.bounds = self.get_surface()

    def get_surface(self):
        text_surface = self.font.render(self.text_func(), False, self.color)
        return text_surface, text_surface.get_rect()

    def draw(self, color):
        text_surface, self.bounds = self.get_surface()
        self._surface.blit(text_surface, self.pos)


class TextBox(DrawableObject):
    def __init__(
            self,
            surface: pygame.Surface,
            pos: Position = Position(0, 0),
            font: str = "Arial",
            font_size: int = 18,
            color: str = "white",
    ):
        super().__init__(surface)
        self.color: str = color
        self.font: str = font
        self.__units: List[_TextBoxUnit] = []
        self.__next_unit_pos: Position = pos
        self.__pos_offset: int = 0
        self.font_size: int = font_size

    def add(self, text_func):
        unit_pos = Position(
            self.__next_unit_pos.x, self.__next_unit_pos.y + self.__pos_offset
        )
        self.__units.append(
            _TextBoxUnit(
                self._surface,
                unit_pos,
                text_func,
                self.color,
                self.font,
                self.font_size,
            )
        )
        self.__pos_offset += self.font_size

    def draw(self, color):
        for unit in self.__units:
            unit.draw(color)
