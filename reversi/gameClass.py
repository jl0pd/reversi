import pygame
from pygame.colordict import THECOLORS

from reversi.gameObjects import Position, Board, TextBox


class Game(object):
    def __init__(self, width: int = 401, height: int = 500, frame_rate: int = 30):
        self.__frame_rate = frame_rate
        self.__game_over = False
        self.__active_player = 0

        pygame.init()
        self.__configure_pygame()

        self.__surface = pygame.Surface((width, height))
        self.__window = pygame.display.set_mode((width, height))
        pygame.display.set_caption("Reversi (Othello)")
        self.__clock = pygame.time.Clock()

        self.__board: Board = Board(self.__surface)
        self.__text_box: TextBox = TextBox(
            self.__surface, Position(0, 405), font="Pagul", font_size=30
        )
        self.__text_box.add(
            lambda: f"Active Player: {'Black' if self.__active_player else 'White'}"
        )
        self.__text_box.add(
            lambda: f"Score: W>{self.__board.score[0]: <2}:{self.__board.score[1]: >2}<B"
        )
        self.__text_box.add(
            lambda: f"Time: {pygame.time.get_ticks() // 60000 :02d}:{pygame.time.get_ticks() // 1000 % 60 :02d}"
        )

    def __change_player(self):
        self.__active_player = 1 - self.__active_player

    def __redraw(self):
        self.__window.blit(self.__surface, (0, 0))
        self.__surface.fill(THECOLORS["green4"])

        self.__board.draw("gray45")
        self.__text_box.draw("white")

        pygame.display.update()

    @staticmethod
    def __configure_pygame():
        ban_list = (
            pygame.MOUSEMOTION,
            pygame.MOUSEBUTTONDOWN,
            pygame.MOUSEBUTTONUP,
            pygame.KEYUP
        )
        pygame.event.set_blocked(ban_list)

    def __handle_events(self):
        for event in pygame.event.get():
            if hasattr(event, "key"):
                if event.key in (pygame.QUIT, pygame.K_ESCAPE, pygame.K_d):
                    self.__game_over = True

            keys = pygame.key.get_pressed()
            if keys[pygame.K_UP]:
                self.__board.cursor.up()
            elif keys[pygame.K_DOWN]:
                self.__board.cursor.down()
            elif keys[pygame.K_LEFT]:
                self.__board.cursor.left()
            elif keys[pygame.K_RIGHT]:
                self.__board.cursor.right()
            elif keys[pygame.K_SPACE]:
                self.__active_player, self.__game_over = self.__board.handle_move(
                    self.__active_player
                )

    def play(self):
        while not self.__game_over:
            self.__handle_events()
            self.__redraw()
            self.__clock.tick(self.__frame_rate)
        pygame.quit()
